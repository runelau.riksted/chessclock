import tkinter as tk
import datetime
import time
import csv
import os
os.chdir(os.path.abspath(os.path.dirname(__file__)))
print(datetime.datetime.today())

root = tk.Tk(className='chess_clock')
root.iconphoto(root._w,tk.PhotoImage(file='logo48.png'))
root.title("ChessClock")
root.tmax = 0
root.count_up = 1
root.t = None
root.started = False
root.Names = []
root.player = 0
root.T = [root.tmax for _ in root.Names]
root.Lost = False
root.Paused = False
root.In_setup = False
root.switching = False
root.sleep_id = None

        
def t_format(t):
    time_str = ''
    non_zero = False
    
    factors = [3600, 60, 1]
    
    t = round(t)
    for factor in factors:
        d = t//factor
        if d or non_zero:
            non_zero = True
            time_str += f'{d:.0f}:'
            t = t%factor
         
    if non_zero:
        time_str = time_str[:-1]
    else:
        time_str = '0'
        
    return time_str

def setup_start():
    if root.In_setup: return
    
    root.In_setup = True
    Main_frame.pack_forget()
    setup_frame.pack()
    setup_name_entry.focus()
    prompt_str.set('Setup players')
    setup_name_entry.bind("<Return>", add_name)
    setup_name_entry.unbind(switch)
    
    for label in root.Player_labels:
        label.bind('<Double-Button-1>', del_player_event_handler)
    
    
def setup_end():
    Main_frame.pack()
    setup_frame.pack_forget()
    
    setup_name_entry.unbind(add_name)
    
    try:
        root.tmax = int(tmax_entry.get())
    except ValueError:
        root.tmax = 0
    
    if root.tmax <= 0:
        root.count_up = 1
    else:
        root.count_up = -1
        
        
    root.focus()
    root.In_setup = False
    
    prompt_str.set('Press SPACE to switch')
    
    
    N = len(root.Names)
    root.T += (N - len(root.T))*[root.tmax]
    time_var.set(t_format(root.T[root.player]))
    name.set(root.Names[root.player])
        
    for i in range(0,N):
        set_p_label(i)
    
    
def unblock_switch():
    root.switching = False

def switch(*event, target = 'next'):
    if event:
        target = event[0].__dict__['char']
        if not len(target) == 1 or not target in ' 0123456789':
            return
        elif target == '0' and not root.In_setup:
            pause() #There is some book keeping, that needs to be done by pause().
            return
        elif target == ' ':
            target = 'next'
        
    if not target==None and (root.In_setup or not root.Names):
        return
    if root.switching:
        return
    else:   
        root.switching = True
        root.after(80, unblock_switch)
    if not root.t == None:
        root.T[root.player] += root.count_up*(time.time() - root.t)
        root.t = time.time()
    
    if not root.Lost:
        set_p_label(root.player)
        if target=='next':
            root.player = (root.player + 1)%len(root.Names)
        elif target == None:
            root.t = None
        elif target in '123456789':
            next_payer = int(target) -1 #minus one to make '1' the first entry i.e. index 0
            if next_payer < len(root.Names):
                root.player = next_payer
            

        if not root.t == None:
            time_var.set(t_format(root.T[root.player] + root.count_up*(time.time() - root.t)))
        else:
            time_var.set(t_format(root.T[root.player]))
        name.set(root.Names[root.player])
    
    
def bell():
    def _bell(n):
        root.bell()
        if n<10:
            root.after(500, _bell, n+1)
    
    _bell(0)
    
    
def count():
    root.sleep_id = root.after(1000, count)
    #root.T[root.player] += root.increment
    
    
    if root.T[root.player] - root.count_up*(root.t-time.time()) <= 0 or root.Lost:
        root.Lost = True
        root.T[root.player] = 0
        time_var.set('Lost')
        root.after_cancel(root.sleep_id)
        root.sleep_id = None
        bell()
        
    else:
        time_var.set(t_format(root.T[root.player] + root.count_up*(time.time()-root.t)))

def start(*event):
    
    if not root.Names:
        return
    if root.started:
        if event:
            return
        else:
            save(add_old=True)
        
    if root.Paused:
        pause() 
    root.T = [root.tmax for _ in root.Names]
    N = len(root.Names)
    for i in range(0,N):
        set_p_label(i)
    
    root.t = time.time()
    root.Lost = False
    
    if root.sleep_id:
        root.after_cancel(root.sleep_id)
        root.sleep_id = None
    
    root.sleep_id = root.after(1000, count)
    root.started = True
    re_start_str.set('Restart')
    time_var.set(t_format(root.tmax))
    
def set_p_label(i):
    root.Player_labels[i].config(text=f' {root.Names[i]} \n{t_format(root.T[i])}')

def del_player(i):
    if i <= root.player and not root.player == 0:
        root.player -= 1
        
    root.Player_labels[i].destroy()
    del root.Player_labels[i]
    del root.Names[i]
    if i < len(root.T):
        del root.T[i]

def del_player_event_handler(event):
    for n, label in enumerate(root.Player_labels):
        if event.__dict__['widget'] == label:
            del_player(n)
    
def add_name(*event, name=None ):
    if name == None:
        name = setup_name_entry.get()
        setup_name_entry.delete( 0, last=tk.END)
            
        
    i = len(root.Names)
    print(f'i={i}')
    root.Names.append(name)
    root.Player_labels.append(tk.Label(Player_frame, text=f' {root.Names[-1]}\n', font=("Arial", 50)))
    root.Player_labels[i].pack(side=tk.LEFT)
         
    root.Player_labels[i].bind('<Double-Button-1>', del_player_event_handler)
    
    max_width = 3
    for name in root.Names:
        if max_width < len(name):
            max_width = len(name)
    max_width += 1
    #for label in root.Player_labels:
    #    label.config(width= max_width)
    
    Label_n.config(width= max_width)
        

def pause(*event):
    
    if event and root.In_setup or root.Lost:
        return
        
    if not root.Names:
        return
        
    if root.Paused:
        root.sleep_id = root.after(1000, count)
        root.t = time.time()
        pause_str.set('Pause')
    else:
        root.after_cancel(root.sleep_id)
        root.sleep_id = None
        switch(target=None)
        pause_str.set('Unpause')
    
    root.Paused = not root.Paused
    
    
def save(add_old=False):
    if not root.started:
        return
        
    if not root.Paused:
        pause()
    
    file_name = file_name_entry.get()
    
    if not file_name:
        file_name = str(datetime.datetime.today())[0:16]
    
    
    if add_old == True:
        file_name += '_save_file_old.csv'
    else:
        file_name += '_save_file.csv'
        
    with open(file_name, 'w') as file:
        writer = csv.writer(file)
        
        writer.writerow(root.Names)
        writer.writerow(root.T)
        writer.writerow([root.tmax])

def load(file_name):
    
    if root.started:
        save()
    for _ in range(0, len(root.Names)):
        del_player(0)   # A new Entry will become index 0 each time, thus deleting the whole list.
        
        
    file_name_entry.delete(0, 1000)
    file_name_entry.insert(0, file_name[0:(-len('_save_file.csv'))])
    root.Paused = True
    root.started = True
    pause_str.set('Unpause')
    re_start_str.set('Restart')
    with open(file_name, 'r') as file:
        reader = csv.reader(file)
        for name in next(reader):
            add_name(name=name)
        root.T = [float(num) for num in next(reader)]
        print(f'root.Names = {root.Names}, root.T = {root.T}')
        try:
            root.tmax = next(reader)[0]
            tmax_entry.delete( 0, last=tk.END)
            tmax_entry.insert(0, root.tmax)
        except StopIteration:
            pass

def destroy():
    save()
    root.quit()
        
        

time_var = tk.StringVar()
name = tk.StringVar()
prompt_str= tk.StringVar()

Player_frame = tk.Frame(root)
Player_frame.pack()
root.Player_labels = []


Button_frame = tk.Frame(root)

Main_frame = tk.Frame(root)
Label_n = tk.Label(Main_frame, textvariable= name, font=("Arial", 200) )
Label = tk.Label(Main_frame, textvariable= time_var, font=("Arial", 200) )
Prompt_Label = tk.Label(root, width = 40, textvariable= prompt_str, font=("Arial", 20) )
root.bind("<Key>", switch)
root.bind("s", start)
root.bind("p", pause)




Label_n.pack()
Label.pack()
Main_frame.pack()
Prompt_Label.pack()
Button_frame.pack(side=tk.BOTTOM)

re_start_str = tk.StringVar()
re_start_str.set('Start')
Reset_button = tk.Button(Button_frame, textvariable=re_start_str, command=start)
Setup_button = tk.Button(Button_frame, text='Setup', command=setup_start)
Reset_button.pack(side=tk.LEFT)
Setup_button.pack(side=tk.LEFT)

pause_str= tk.StringVar()
pause_str.set('Pause')
tk.Button(Button_frame, textvariable=pause_str, command=pause).pack(side=tk.LEFT)





setup_frame = tk.Frame(root)
tk.Label(setup_frame, text='Enter Name:').pack()
setup_name_entry = tk.Entry(setup_frame)
setup_name_entry.pack()
tk.Button(setup_frame, text='Add Name', command=add_name).pack()
tk.Label(setup_frame, text='Number of seconds to count down from:').pack()
tmax_entry = tk.Entry(setup_frame, width=3)
tmax_entry.insert(0, str(root.tmax))
tmax_entry.pack()

tk.Label(setup_frame, text='File name:').pack()
file_name_entry = tk.Entry(setup_frame, width=15)
file_name_entry.pack()

saved_files = tk.Frame(setup_frame)
tk.Label(saved_files, text='Load file:').pack()
for file_name in os.listdir():
    if '_save_file.csv' in file_name:
        def generator(name):
            return lambda: load(name)
        
        tk.Button(saved_files, text=file_name[0:(-len('_save_file.csv'))], command=generator(file_name)).pack(side=tk.LEFT)
        
saved_files.pack()
tk.Button(setup_frame, text='End setup', command=setup_end).pack()


prompt_str.set('Press SPACE to switch')
setup_start()
root.destroy = destroy
root.protocol("WM_DELETE_WINDOW", root.destroy)

root.mainloop()
